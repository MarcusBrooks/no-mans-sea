package uk.co.greedygull.main.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.main.Main;

public class DesktopLauncher {

	private static final boolean packTextures = true;

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = Global.WIDTH;
        config.height = Global.HEIGHT;

		if(packTextures){
			Settings settings = new Settings();
			settings.maxWidth = 2048;
			settings.alphaThreshold = 255;
			settings.debug = false;

			TexturePacker.process(settings,"images","atlasAssets","uiAssets");
		}

		new LwjglApplication(new Main(), config);

	}
}
