package uk.co.greedygull.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.server.Network;

/**
 * Created by Matej on 29/04/2015
 */
public class JoinScreen extends AbstractScreen {

    private Label title = new Label("JOIN GAME", menuSkin);
    private TextField address = new TextField("ENTER IP", menuSkin);
    private TextButton joinBtn = new TextButton("JOIN", menuSkin),
            backBtn = new TextButton("BACK", menuSkin);


    @Override
    public void show() {
     super.show();
        title.setPosition(Global.WIDTH / 2 - title.getWidth()/2,Global.HEIGHT - 100);

        address.setSize(750,150);
        address.setPosition(Global.WIDTH/2 - address.getWidth()/2, Global.HEIGHT - 300);

        joinBtn.setSize(300, 150);
        joinBtn.setPosition(Global.WIDTH / 2 - joinBtn.getWidth()/2, Global.HEIGHT / 2 - 150);

        backBtn.setPosition(50, 0);

        stage.addActor(title);
        stage.addActor(address);
        stage.addActor(joinBtn);
        stage.addActor(backBtn);

        joinBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("JOIN CLICKED");
                ((Game) Gdx.app.getApplicationListener()).setScreen(new HostScreen(false));
            }
        });

        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("BACK CLICKED");
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
            }
        });


    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
