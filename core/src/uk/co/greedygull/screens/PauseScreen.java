package uk.co.greedygull.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Matej on 14/05/2015
 */
public class PauseScreen {

    private Window pauseWindow, confirmWindow;
    private Table table;
    private Label skillPoints;
    private ImageButton hpAdd, speedAdd, attackAdd, defenceAdd, btn_mute, btn_quit, btn_yes, btn_no;
    private Image hpIcon, speedIcon, attackIcon, defenceIcon;
    private ProgressBar hpBar, speedBar, attackBar, defenceBar;

    public int sp = 13;
    private float pad = 30;


    public void createPaused(Skin skin, Stage stage){

        //Create Pause Window
        pauseWindow = new Window("pauseWindow",skin);
        pauseWindow.padTop(20f);
        pauseWindow.setSize(skin.getRegion("pauseBackground").getRegionWidth(), skin.getRegion("pauseBackground").getRegionHeight());
        pauseWindow.setMovable(true);
        pauseWindow.setPosition((Gdx.graphics.getWidth() / 2) - (pauseWindow.getWidth() / 2), 20);

        //Create Quit confirmation window
        confirmWindow = new Window("confirmWindow",skin,"confirm");
        confirmWindow.padTop(20f);
        confirmWindow.setSize(skin.getRegion("confirmBackground").getRegionWidth(), skin.getRegion("confirmBackground").getRegionHeight());
        confirmWindow.setMovable(true);
        confirmWindow.setPosition(pauseWindow.getX() + (pauseWindow.getWidth() / 4), pauseWindow.getY() + pauseWindow.getHeight() / 4);
        confirmWindow.toFront();

        //Create confirm button
        btn_yes = new ImageButton(skin, "yesButton");
        confirmWindow.add(btn_yes).pad(pad*2, 0, 0, pad);

        //Create cancel button
        btn_no = new ImageButton(skin, "noButton");
        confirmWindow.add(btn_no).pad(pad*2,pad,0,0);

        //Create mute button
        btn_mute = new ImageButton(skin, "muteButton");

        //Create quit button
        btn_quit = new ImageButton(skin, "quitButton");

        //Create available skill points label
        skillPoints = new Label("",skin);
        skillPoints.setText("" + sp);
        skillPoints.setFontScale(0.5f);

        //Create skills table
        table = new Table(skin);
        //table.setSize(pauseWindow.getWidth(), pauseWindow.getHeight() - 180);

        //Create skill icons
        hpIcon = new Image(skin.getDrawable("hpIcon"));
        speedIcon = new Image(skin.getDrawable("speedIcon"));
        attackIcon = new Image(skin.getDrawable("attackIcon"));
        defenceIcon = new Image(skin.getDrawable("defenceIcon"));

        //Create skill bars
        hpBar = new ProgressBar(0,100,10,false,skin, "skills");
        hpBar.setSize(655,skin.getRegion("uiBar").getRegionHeight());

        speedBar = new ProgressBar(0,100,10,false,skin, "skills");
        //speedBar.setSize(skin.getRegion("uiBar").getRegionWidth(),skin.getRegion("uiBar").getRegionHeight());

        attackBar = new ProgressBar(0,100,10,false,skin, "skills");
        //attackBar.setSize(skin.getRegion("uiBar").getRegionWidth(),skin.getRegion("uiBar").getRegionHeight());

        defenceBar = new ProgressBar(0,100,10,false,skin, "skills");
        //defenceBar.setSize(skin.getRegion("uiBar").getRegionWidth(),skin.getRegion("uiBar").getRegionHeight());

        //Create skill add buttons
        hpAdd = new ImageButton(skin, "addButton");
        speedAdd = new ImageButton(skin, "addButton");
        attackAdd = new ImageButton(skin, "addButton");
        defenceAdd = new ImageButton(skin, "addButton");



        //Add all skill elements into table
        table.add(hpIcon); table.add(hpBar); table.add(hpAdd);
        table.row().pad(pad, 0, 0, 0);
        table.add(speedIcon); table.add(speedBar); table.add(speedAdd);
        table.row().pad(pad, 0, 0, 0);
        table.add(attackIcon); table.add(attackBar); table.add(attackAdd);
        table.row().pad(pad, 0, 0, 0);
        table.add(defenceIcon); table.add(defenceBar); table.add(defenceAdd);
        //table.debug();

        pauseWindow.row().expandX();
        pauseWindow.add(skillPoints).pad(0, pad * 2, 0, 0).right();
        pauseWindow.row();
        pauseWindow.add(table).pad(pad,pad * 2,pad,0).center();
        pauseWindow.row();
        pauseWindow.add(btn_quit).pad(0,pad,0,0).left();
        pauseWindow.add(btn_mute).pad(0,0,0,pad).right();
        //pauseWindow.debug();

        pauseWindow.setVisible(false);
        confirmWindow.setVisible(false);

        //Add all actors to stage
        stage.addActor(pauseWindow);
        stage.addActor(confirmWindow);

        //Input listeners for ALL BUTTONS
        hpAdd.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("HP ADD CLICKED");
                if (sp > 0) {
                    hpBar.setValue(hpBar.getValue() + 10);
                    updateSP(sp - 1);
                }
            }
        });

        speedAdd.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("SPEED ADD CLICKED");
                if(sp > 0){
                    speedBar.setValue(speedBar.getValue() + 10);
                    updateSP(sp-1);
                }
            }
        });

        attackAdd.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("ATTACK ADD CLICKED");
                if(sp > 0){
                    attackBar.setValue(attackBar.getValue() + 10);
                    updateSP(sp-1);
                }
            }
        });

        defenceAdd.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("HP ADD CLICKED");
                if(sp > 0){
                    defenceBar.setValue(defenceBar.getValue() + 10);
                    updateSP(sp-1);
                }
            }
        });

        btn_mute.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("MUTE CLICKED");
                    updateSP(sp+1);
                    //TODO Add actual mute, not cheeky infinite skill points

            }
        });

        btn_quit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("QUIT CLICKED");
                confirmWindow.toFront();
                if(!confirmWindow.isVisible()){
                    confirmWindow.setVisible(true);
                    pauseWindow.setMovable(false);
                }
            }
        });

        btn_no.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("QUIT CLICKED");
                if(confirmWindow.isVisible()){
                    confirmWindow.setVisible(false);
                    pauseWindow.setMovable(true);
                }
            }
        });

        btn_yes.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("QUIT CONFIRM CLICKED");
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
                //TODO Quit properly, currently crashes upon joining another game after quitting
            }
        });



    }

    public void pause(){
        if(pauseWindow.isVisible()){
            pauseWindow.setVisible(false);
            confirmWindow.setVisible(false);
        } else if(!pauseWindow.isVisible()){
            pauseWindow.setVisible(true);
        }

    }

    public void updateSP(int newSP){
        sp = newSP;
        skillPoints.setText(""+sp);
    }

}
