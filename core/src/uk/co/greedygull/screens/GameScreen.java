package uk.co.greedygull.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ai.steer.behaviors.Wander;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import uk.co.greedygull.entities.client.CEntity;
import uk.co.greedygull.entities.client.CGreenEntity;
import uk.co.greedygull.entities.server.SteerableEntity;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.entities.client.C_Player;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import uk.co.greedygull.entities.client.CGreenEntity;
import uk.co.greedygull.entities.client.C_Player;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.map.Map;
import uk.co.greedygull.render.DebugRenderer;
import uk.co.greedygull.render.Renderable;
import uk.co.greedygull.render.Renderer;
import uk.co.greedygull.server.*;

import static uk.co.greedygull.main.Global.*;

/**
 * Created by Marcus on 03/04/2015.
 */
public class GameScreen extends AbstractScreen {

    private DebugRenderer debug;
    private Box2DDebugRenderer debug2d;
    private Renderer renderer;
    private OrthographicCamera camera;
    private Map map;
    private Network network;
    private static long playerID;

    private Array<C_Player> players;
    private C_Player player;

    Array<CGreenEntity> entities;
    Array<Vector2> clickPositions;



    public GameScreen(Network network){
        this.network = network;
        network.updateServer(Gdx.graphics.getDeltaTime());
        initNetwork();
    }

    public void initNetwork(){
        network.send("map");
        network.send("entities");
        network.send("player");

        players = new Array<C_Player>();
        BasicPlayer playerResponse = network.getInitialPlayer();

        playerID = playerResponse.id;
        player = new C_Player(playerResponse.x, playerResponse.y, playerResponse.id);
        player.loadAssets();
        updatePlayers();

        entities = new Array<CGreenEntity>();

        for(BasicEntity e : network.getEntities().entities){
            entities.add(new CGreenEntity(e.id, e.x, e.y, e.rotation));
        }

        map = new Map(network.getMap());
        map.loadAssets();
    }

    public void updatePlayers(){
        BasicPlayer playerRequest = new BasicPlayer();
        playerRequest.x = 0;
        playerRequest.y = 0;
        playerRequest.id = playerID;
        network.send(playerRequest);

        PlayerResponse playerResponse = network.getPlayerReponse();
        for (int i = 0; i < playerResponse.players.length; i++) {

            if(playerResponse.players[i].id == playerID){

            } else {
               C_Player p = new C_Player(playerResponse.players[i].x,playerResponse.players[i].y,playerResponse.players[i].id);
                players.add(p);

            }

        }
    }

    @Override
    public void show() {
        super.show();
        camera = new OrthographicCamera(CAMERA_WIDTH, CAMERA_HEIGHT);
        camera.translate(CAMERA_WIDTH / 2, CAMERA_HEIGHT / 2);
        camera.zoom = CAMERA_ZOOM;
        camera.update();

        renderer = new Renderer();
        renderer.loadAssets();

        debug = new DebugRenderer();
        debug2d = new Box2DDebugRenderer();



        clickPositions = new Array<Vector2>();

        addJoystick();
        loadUI();
    }


    private float entityDelay = 0.0f;
    private float playerDelay = 0.0f;
    public void update(final float delta){
//        player.updateAnimation(delta);
//        player.updateMovement(new Vector2(touchpad.getKnobPercentX(), touchpad.getKnobPercentY()));
//        camera.position.x =player.x;
//        camera.position.y =player.y;
//        camera.update();

        network.updateServer(delta);
//               network.send("entities");
//        entities = new Array<CGreenEntity>(); //TODO

        playerDelay += delta;
        if(playerDelay > Global.PLAYER_UPDATE_DELAY){
            playerDelay -= Global.PLAYER_UPDATE_DELAY;

            BasicPlayer playerRequest = new BasicPlayer();

            boolean pressed = false;

            if(Gdx.input.isKeyPressed(Input.Keys.UP)){
                playerRequest.x = 0;
                playerRequest.y = 1;
                pressed = true;
            }
            if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
                playerRequest.x = 0;
                playerRequest.y = -1;
                pressed = true;

            }
            if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
                playerRequest.x = -1;
                playerRequest.y = 0;
                pressed = true;

            }
            if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
                playerRequest.x = 1;
                playerRequest.y = 0;
                pressed = true;

            }

            if(!pressed) {
                playerRequest.x = touchpad.getKnobPercentX();
                playerRequest.y = touchpad.getKnobPercentY();
            }

            if(Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)){
                AttackRequest attackRequest = new AttackRequest();
                attackRequest.id = playerID;
                network.send(attackRequest);
            }

            playerRequest.id = playerID;
            network.send(playerRequest);
            PlayerResponse playerResponse = network.getPlayerReponse();
            for (int i = 0; i < playerResponse.players.length; i++) {

                if(playerResponse.players[i].id == playerID){
                    player.x = playerResponse.players[i].x;
                    player.y = playerResponse.players[i].y;

                    camera.position.x = player.x;
                    camera.position.y = player.y;
                    camera.update();

                    if(player.updateMovement()){
                        player.updateAnimation(delta);
                    }
                } else {
                    boolean exists = false;
                    for (int j = 0; j < players.size; j++) {

                        if(players.get(j).id == playerResponse.players[i].id){
                            players.get(j).x = playerResponse.players[i].x;
                            players.get(j).y = playerResponse.players[i].y;
                            exists = true;
                        }

                        if(players.get(j).updateMovement()){
                            players.get(j).updateAnimation(delta);
                        }
                    }

                    if(!exists){
                        C_Player p = new C_Player(playerResponse.players[i].x,playerResponse.players[i].y,playerResponse.players[i].id);
                        players.add(p);
                    }

                }

            }
        }
//        network.getServer().g
        //Want to get all the ray<Vector2> 's from all GreenEntities
//        rays=network.getServer().getRayss();


//        entityDelay += delta;
//        if(entityDelay > Global.ENTITY_UPDATE_DELAY){
//            entityDelay -= Global.ENTITY_UPDATE_DELAY;
            network.send("entities");
//
//            for(BasicEntity e : network.getEntities().entities){
//                for (int i = 0; i < entities.size; i++) {
//                    if(entities.get(i).id == e.id){
//
//                        entities.set(i, new CGreenEntity(e.id, e.x, e.y, e.rotation));
////                        entities.get(i).x = e.x;
////                        entities.get(i).y = e.y;
//
//
//                    }
//                }
//            }
//        }

        entities = new Array<CGreenEntity>();

        for(BasicEntity e : network.getEntities().entities){
            if(e!=null)
                entities.add(new CGreenEntity(e.id, e.x, e.y, e.rotation));
        }
    }

    @Override
    public void render(final float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        update(delta);

        renderer.setProjectionMatrix(camera.combined);
        renderer.render(map);

//        debug.setProjectionMatrix(camera.combined);
        //debug.render(map);

//        debug2d.render(GameServer.world, camera.combined);

        renderer.setProjectionMatrix(camera.combined);
        renderer.render(entities);

        for (int i = 0; i < players.size; i++) {
            renderer.render(players.get(i));
        }

        renderer.render(player);
//        debug.setProjectionMatrix(camera.combined);

//        debug.render(entities);


//        if (Gdx.input.isTouched()) {
//            clickPositions.add(new Vector2(Gdx.input.getX(), Global.HEIGHT - Gdx.input.getY()));
//            renderer.circle(Gdx.input.getX(), Gdx.input.getY(), 10);
//        }
//        if(clickPositions.size > 0) {
//            debug.begin(ShapeRenderer.ShapeType.Filled);
//            for (int i = 0; i < clickPositions.size; i++) {
//                debug.circle(clickPositions.get(i).x,Global.CAMERA_HEIGHT-clickPositions.get(i).y, 10);
//            }
//            debug.end();
//        }

        stage.draw();
    }

    /**
     * Creates the joystick
     */
    private Touchpad touchpad;
    private Touchpad.TouchpadStyle touchpadStyle;
    private Skin touchpadSkin;
    private Drawable touchBackground;
    private Drawable touchKnob;
    public void addJoystick(){
        //Create a skin to hold the joystick graphics1
        touchpadSkin = new Skin();
        //Set joystick boundary image
        touchpadSkin.add("touchBackground", new Texture(Gdx.files.internal("joystick/touchBackground.png")));
        //Set joystick image
        touchpadSkin.add("touchKnob", new Texture(Gdx.files.internal("joystick/joystick.png")));
        //Create TouchPad Style
        touchpadStyle = new Touchpad.TouchpadStyle();
        //Create Drawable's from TouchPad skin
        touchBackground = touchpadSkin.getDrawable("touchBackground");
        touchKnob = touchpadSkin.getDrawable("touchKnob");
        //Apply the Drawables to the TouchPad Style
        touchpadStyle.background = touchBackground;
        touchpadStyle.knob = touchKnob;
        //Create new TouchPad with the created style
        touchpad = new Touchpad(10, touchpadStyle);
        //setBounds(x,y,width,height)
        touchpad.setBounds(15, 15, 200, 200);

        //Create a Stage and add TouchPad
        stage.addActor(touchpad);

    }


    private ImageButton btn_A, btn_B, btn_pause;
    private Image playerBar;
    private TextButton playerIcon;
    private ProgressBar playerHealth, playerExp;

    private PauseScreen paused;

    private int pLevel = 1; //TODO add player level
    private int pHealth = 100; //TODO add player health


    public void loadUI(){

        btn_A = new ImageButton(uiSkin,"aButton");
        btn_A.setPosition(Gdx.graphics.getWidth() - btn_A.getWidth() - 150 ,50);

        btn_B = new ImageButton(uiSkin, "bButton");
        btn_B.setPosition(Gdx.graphics.getWidth() - btn_B.getWidth() - 50, 150);

        btn_pause = new ImageButton(uiSkin, "pauseButton");
        btn_pause.setPosition(Gdx.graphics.getWidth() - btn_pause.getWidth() - 20,
                Gdx.graphics.getHeight() - btn_pause.getHeight() - 20);

        playerIcon = new TextButton(""+pLevel,uiSkin,"playerEmblem");
        playerIcon.getStyle().font.setScale(0.5f);
        playerIcon.setPosition(10, Gdx.graphics.getHeight() - playerIcon.getHeight() - 10);

        playerBar = new Image(uiSkin.getDrawable("playerBar"));
        playerBar.setPosition(40, Gdx.graphics.getHeight() - playerBar.getHeight() - 20);

        playerHealth = new ProgressBar(0f,100,1f,false,uiSkin, "healthBar");
        playerHealth.setWidth(uiSkin.getRegion("healthBar").getRegionWidth());
        playerHealth.setValue(pHealth);
        playerHealth.setPosition(playerBar.getX() + 29, playerBar.getY() + 45);
        playerHealth.getStyle().knobBefore.setMinWidth(1);

        playerExp = new ProgressBar(0f,100,1f,false,uiSkin, "expBar");
        playerExp.setWidth(uiSkin.getRegion("expBar").getRegionWidth());
        playerExp.setValue(0);
        playerExp.setPosition(playerBar.getX() + 18, playerBar.getY() + 17);
        playerExp.getStyle().knobBefore.setMinWidth(1);


        stage.addActor(btn_A);
        stage.addActor(btn_B);
        stage.addActor(btn_pause);
        stage.addActor(playerBar);
        stage.addActor(playerHealth);
        stage.addActor(playerExp);
        stage.addActor(playerIcon);

        paused = new PauseScreen();
        paused.createPaused(uiSkin,stage);

        btn_pause.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("PAUSE CLICKED");
                paused.pause();
            }
        });

        btn_A.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                AttackRequest attackRequest = new AttackRequest();
                attackRequest.id = playerID;
                network.send(attackRequest);
            }
        });

    }

    public void changeHealth(int hp){
        playerHealth.setValue(playerHealth.getValue() + hp);

        if(playerHealth.getValue() < 1){
            //TODO PLAYER DEATH
        }

    }

    public void addExp(int xp){
        playerExp.setValue(playerExp.getValue()+xp);
        if(playerExp.getValue()>=100){
            pLevel++;
            playerIcon.setText(""+pLevel);
            paused.updateSP(paused.sp + 1);
            playerExp.setValue(0);
            playerHealth.setValue(100);
        }
    }

    @Override
    public void resize(final int width, final int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
