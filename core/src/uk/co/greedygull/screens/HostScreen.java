package uk.co.greedygull.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.server.*;

import java.util.ArrayList;

/**
 * Created by Matej on 29/04/2015
 */
public class HostScreen extends AbstractScreen {
    private Network network;
    private boolean isHost;

    public HostScreen(boolean isHost){
        //Sets up the network interface
        this.isHost = isHost;
    }

    @Override
    public void show() {
        super.show();
        startNetwork(isHost);
        //Pings the network to check that it is working
        network.testNetwork();

        createUI();
    }

    private Label title, address, playerName;
    private Table players, playerTbl;
    private TextButton playBtn,backBtn, playerEmblem;

    private ArrayList<ImageButton> ready = new ArrayList<ImageButton>();

    public int numPlayers = 2; //TODO GET PLAYERS

    public Table addPlayer(int id, String name, String lvl){

        playerTbl = new Table();
        playerTbl.background(uiSkin.getDrawable("HostPlayerBG"));
        playerName = new Label(name.toUpperCase(), uiSkin);
        playerEmblem = new TextButton(lvl, uiSkin, "playerEmblem");
        playerEmblem.getStyle().font.setScale(0.5f);
        ready.add(new ImageButton(uiSkin,"readyButton"));

        playerTbl.add(playerEmblem).pad(15, 15, 15, 15).left();
        playerTbl.add(playerName).size(Gdx.graphics.getWidth()/2,playerName.getHeight());
        playerTbl.add(ready.get(id)).padRight(15).right();
        playerTbl.row();
        // playerTbl.debug();

        return playerTbl;
    }

    private void createUI() {

        title = new Label("LOBBY", menuSkin);
        title.setPosition(Global.WIDTH / 2 - title.getWidth() / 2, Global.HEIGHT - 150);

        address = new Label("IP 86 10 167 98", menuSkin);
        address.setSize(500, 150);
        address.setFontScale(0.3f);
        address.setPosition(50, Global.HEIGHT - title.getHeight() - 20);

        players = new Table();
        players.setPosition(Gdx.graphics.getWidth() / 2,Gdx.graphics.getHeight()/2);
        players.align(1);
        //players.debug();

        //Populates list of players
        for(int i = 0; i < numPlayers; i++){
            players.add(addPlayer(i,"Player"+(i+1), "1"));
            players.row();
        }

        //Create Play and Back buttons
        playBtn = new TextButton("PLAY", menuSkin);
        playBtn.setSize(300, 100);
        playBtn.setPosition(Global.WIDTH-playBtn.getWidth()-50, 0);

        backBtn = new TextButton("BACK", menuSkin);
        backBtn.setPosition(50, 0);

        //Add all elements into the stage
        stage.addActor(title);
        stage.addActor(address);
        stage.addActor(playBtn);
        stage.addActor(backBtn);

        stage.addActor(players);

        //Create click listeners for the buttons
        playBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("JOIN CLICKED");
                boolean allReady = true;
                for(int i = 0; i < numPlayers; i++){
                    if(!ready.get(i).isChecked()) {
                        allReady = false;
                    }
                }

                if(allReady){
                    ((Game) Gdx.app.getApplicationListener()).setScreen(new GameScreen(network));
                } else {
                    System.out.println("NOT ALL PLAYERS READY");
                }

            }
        });

        backBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("BACK CLICKED");
                ((Game) Gdx.app.getApplicationListener()).setScreen(new MenuScreen());
            }
        });
    }

    /**
     * Initialises the network
     * @param isHosting - Defines if this client is also hosting the game
     */
    private void startNetwork(final boolean isHosting) {
        network = new Network(isHosting);
        if(isHosting) {
            network.registerServerClass(TextRequest.class);
            network.registerServerClass(TextResponse.class);

            network.registerServerClass(EntityResponse.class);
            network.registerServerClass(EntityResponse.class);
            network.registerServerClass(AttackRequest.class);

            network.registerServerClass(PlayerResponse.class);
//            network.registerServerClass(PlayerRequest.class);

            network.registerServerClass(BasicEntity.class);
            network.registerServerClass(BasicEntity[].class);

            network.registerServerClass(BasicPlayer.class);
            network.registerServerClass(BasicPlayer[].class);

            network.registerServerClass(MapResponse.class);
            network.registerServerClass(byte[][].class);
            network.registerServerClass(byte[].class);

        }
        network.registerClientClass(TextRequest.class);
        network.registerClientClass(TextResponse.class);

        network.registerClientClass(EntityResponse.class);
        network.registerClientClass(EntityResponse.class);
        network.registerClientClass(AttackRequest.class);

        network.registerClientClass(PlayerResponse.class);
//        network.registerClientClass(PlayerRequest.class);

        network.registerClientClass(BasicEntity.class);
        network.registerClientClass(BasicEntity[].class);

        network.registerClientClass(BasicPlayer.class);
        network.registerClientClass(BasicPlayer[].class);

        network.registerClientClass(MapResponse.class);
        network.registerClientClass(byte[][].class);
        network.registerClientClass(byte[].class);

    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
