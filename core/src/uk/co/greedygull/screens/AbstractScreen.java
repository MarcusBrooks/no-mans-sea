package uk.co.greedygull.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created by Matej on 23/04/2015
 */
public class AbstractScreen implements Screen{

    protected Stage stage;
    protected TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("atlasAssets/uiAssets.atlas"));
    protected TextureAtlas menuAtlas = new TextureAtlas(Gdx.files.internal("atlasAssets/menuAssets.atlas"));
    protected Skin uiSkin = new Skin(Gdx.files.internal("atlasAssets/uiSkin.json"), atlas);
    protected Skin menuSkin = new Skin(Gdx.files.internal("atlasAssets/menuSkin.json"), menuAtlas);



    @Override
    public void show() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
