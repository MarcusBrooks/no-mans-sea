package uk.co.greedygull.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import uk.co.greedygull.main.Global;

/**
 * Created by Matej on 24/04/2015
 */
public class MenuScreen extends AbstractScreen {

    private Label title = new Label("NO MANS LAND", menuSkin);
    private TextButton hostBtn = new TextButton("HOST", menuSkin),
                        joinBtn = new TextButton("JOIN", menuSkin),
                        exitBtn = new TextButton("QUIT", menuSkin);
    private Image gull = new Image(uiSkin.getDrawable("greedyGull"));



    @Override
    public void show() {
        super.show();
        title.setPosition(Global.WIDTH / 2 - title.getWidth()/2,Global.HEIGHT - 100);

        hostBtn.setSize(300, 150);
        hostBtn.setPosition(Global.WIDTH / 2 - hostBtn.getWidth() / 2, Global.HEIGHT / 2);

        joinBtn.setSize(300, 150);
        joinBtn.setPosition(Global.WIDTH / 2 - joinBtn.getWidth() / 2, Global.HEIGHT / 2 - hostBtn.getHeight() - 50);

        exitBtn.setPosition(50, 0);

        gull.setSize(100,100);
        gull.setPosition(Gdx.graphics.getWidth()-gull.getWidth(),0);

        stage.addActor(title);
        stage.addActor(hostBtn);
        stage.addActor(joinBtn);
        stage.addActor(exitBtn);
        stage.addActor(gull);

        hostBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("HOST CLICKED");
                ((Game) Gdx.app.getApplicationListener()).setScreen(new HostScreen(true));
            }
        });

        joinBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("JOIN CLICKED");
                ((Game) Gdx.app.getApplicationListener()).setScreen(new JoinScreen());
            }
        });

        exitBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent e, float x, float y) {
                System.out.println("EXIT CLICKED");
                Gdx.app.exit();
            }
        });

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
