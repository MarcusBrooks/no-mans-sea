package uk.co.greedygull.render;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by Marcus on 08/04/2015.
 */
public interface Renderable {
    public void render(SpriteBatch renderer);
    public void render(ShapeRenderer renderer);
}
