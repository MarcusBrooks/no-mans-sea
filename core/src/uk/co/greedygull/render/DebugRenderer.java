package uk.co.greedygull.render;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import uk.co.greedygull.entities.client.CGreenEntity;

/**
 * Created by Marcus on 02/04/2015.
 */
public class DebugRenderer extends ShapeRenderer {

    public void render(final Renderable arg) {
        begin(ShapeType.Filled);
            arg.render(this);
        end();
    }

    public void render(final Renderable[] args) {
        begin();
        for (int i = 0; i < args.length; i++) {
            args[i].render(this);
        }
        end();
    }

    public void render(final Array<CGreenEntity> args){
        setAutoShapeType(true);
        begin(ShapeRenderer.ShapeType.Line);
        for (int i = 0; i < args.size; i++) {
            args.get(i).render(this);
        }
        end();
    }


//    public void render(final Array<CGreenEntity> args) {
//        begin();
//        for (int i = 0; i < args.size; i++) {
//            args.get(i).render(this);
//        }
//        end();
//    }
}
