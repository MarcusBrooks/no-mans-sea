package uk.co.greedygull.render;

/**
 * Created by Marcus on 14/04/2015.
 */
public interface Animated {
    public void updateFrames(float delta);
    public int getNumFrames();
}
