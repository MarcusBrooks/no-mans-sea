package uk.co.greedygull.render;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import uk.co.greedygull.entities.client.CEntity;
import uk.co.greedygull.entities.client.CGreenEntity;

/**
 * Created by Marcus on 01/04/2015.
 */
public class Renderer extends SpriteBatch {

    private static Texture img;

    public static final boolean loadAssets() {
        try {
            img = new Texture("greedygull.png");
            CGreenEntity.loadAssets();

            return true;
        } catch (Exception e) {
            System.out.print("Error: Unable to load assets - " + e.getMessage());
            return false;
        }
    }

    public void render(final Renderable arg) {
        begin();
            arg.render(this);
        end();
    }

    public void render(final Renderable[] args) {
        begin();
        for (int i = 0; i < args.length; i++) {
            args[i].render(this);
        }
        end();
    }

    public void render(Array<CGreenEntity> args){
        begin();
        for (int i = 0; i < args.size; i++) {
            args.get(i).render(this);
        }
        end();
    }
}
