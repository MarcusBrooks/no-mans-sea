package uk.co.greedygull.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ai.steer.behaviors.*;
import com.badlogic.gdx.ai.steer.limiters.NullLimiter;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import uk.co.greedygull.components.*;
import uk.co.greedygull.entities.FieldOfView;
import uk.co.greedygull.entities.Moveable;
import uk.co.greedygull.entities.server.GreenEntity;
import uk.co.greedygull.entities.server.ProximityObj;
import uk.co.greedygull.entities.server.SteerableEntity;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.server.GameServer;

/**
 * Created by Marcus on 09/04/2015.
 */
public class AISystem extends IteratingSystem {
    private ImmutableArray<Entity> entities;



    public AISystem() {
        super(Family.all(SizeComponent.class, MovementStateComponent.class, Box2DComponent.class).get());

    }

    @Override
    public void update (float deltaTime) {

//            if(Gdx.input.isTouched()) {
//                SteerableEntity target = new SteerableEntity(GameServer.world, new Vector2(Gdx.input.getX(), Global.CAMERA_HEIGHT - Gdx.input.getY()), 10, true);
//                ProximityObj proximity = new ProximityObj(target, GameServer.world, target.getBoundingRadius() * 4);
//
//                CollisionAvoidance<Vector2> collisionAvoidanceSB = new CollisionAvoidance<Vector2>(target, proximity);//does some crazy shit
//
//            }
        super.update(deltaTime);
    }

    public void processEntity(Entity entity, float deltaTime) {

        if(entity instanceof GreenEntity){
            ((GreenEntity) entity).update(deltaTime);

//            if (Gdx.input.isKeyPressed(Input.Keys.ANY_KEY)) {
//                System.out.println("keyPressed");
//                //((SteerableEntity) entity).getSteeringBehavior().setEnabled(false);
//                ((GreenEntity) entity).setWander();
//            } else {
//
//            }


        }



    }
}
