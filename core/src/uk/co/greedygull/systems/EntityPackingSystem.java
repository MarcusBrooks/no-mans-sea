package uk.co.greedygull.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Vector2;
import uk.co.greedygull.components.*;
import uk.co.greedygull.entities.server.SteerableEntity;
import uk.co.greedygull.server.BasicEntity;
import uk.co.greedygull.server.EntityResponse;

/**
 * Created by Marcus on 14/04/2015.
 */
public class EntityPackingSystem extends EntitySystem {

    public EntityResponse response;
    private ImmutableArray<Entity> entities;


    @Override
    public void addedToEngine(Engine engine) {
        entities = engine.getEntitiesFor(Family.all(SizeComponent.class, MovementStateComponent.class, Box2DComponent.class).get());
    }

    @Override
    public void update(float deltaTime) {

        response = new EntityResponse();
        response.entities = new BasicEntity[entities.size()];
        

        for (int i = 0; i < entities.size(); i++) {

            Entity e = entities.get(i);
            if(e instanceof SteerableEntity) {
                Vector2 position = Mapper.box2d.get(e).body.getPosition();

                response.entities[i] = new BasicEntity();
                response.entities[i].id = e.getId();
                response.entities[i].x = SteerableEntity.metersToPixels(position.x);
                response.entities[i].y = SteerableEntity.metersToPixels(position.y);
                response.entities[i].rotation = Mapper.box2d.get(e).body.getAngle();

            }
        }
    }
}
