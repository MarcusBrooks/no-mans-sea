package uk.co.greedygull.entities.client;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;
import uk.co.greedygull.render.Animated;
import uk.co.greedygull.render.Renderable;

/**
 * Created by Marcus on 14/04/2015.
 */
public class CEntity implements Renderable, Animated {

    public long id;
    public float x;
    public float y;
    protected int size;
    protected float rotation;

    private float time = 0;
    private int currentTexture = 0;

    public Texture[] getTextures(){
        return null;
    }

    @Override
    public void render(SpriteBatch renderer) {
        renderer.draw(getTextures()[currentTexture], x, y, size, size);
    }

    @Override
    public void render(ShapeRenderer renderer) {

    }

    @Override
    public void updateFrames(float delta) {
        time += delta;

        if(time > 1.0f) {
            time = 0;
            currentTexture++;
            if(currentTexture > getNumFrames()){
                currentTexture = 0;
            }
        }
    }

    public int getNumFrames(){
        return getTextures().length;
    }
}
