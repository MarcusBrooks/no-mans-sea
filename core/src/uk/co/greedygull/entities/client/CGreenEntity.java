package uk.co.greedygull.entities.client;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import mikera.math.Vector2;
import uk.co.greedygull.main.Global;

/**
 * Created by Marcus on 14/04/2015.
 */
public class CGreenEntity extends CEntity {

    public static Texture[] textures;


    float centerX;
    float centerY;

    public CGreenEntity(long id, float x, float y, float rotation){
        this.id = id;
        this.x = x;
        this.y = y;
        this.size = Global.TILE_SIZE / 2;
        this.centerX = x - (size/2);
        this.centerY = y - (size/2);
        this.rotation = rotation;

    }

    @Override
    public void render(SpriteBatch renderer) {
        TextureRegion region = new TextureRegion(getTextures()[0]);

        renderer.draw(region, centerX, centerY, size/2, size/2, size, size, 1, 1, (rotation * MathUtils.radiansToDegrees));

    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void render(ShapeRenderer renderer) {
        //Steerable<Vector2> steerable = characters.get(0);





        renderer.setColor(1, 0, 0, 1);
        renderer.circle(x, y, 10);

    }

    public static void loadAssets() {
        textures = new Texture[3];
        textures[0] = (new Texture("Ship1_0.png"));
        textures[1] = (new Texture("Ship1_1.png"));
        textures[2] = (new Texture("Ship1_2.png"));
    }

    public Texture[] getTextures(){
        return textures;
    }


}
