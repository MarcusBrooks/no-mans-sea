package uk.co.greedygull.entities.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import uk.co.greedygull.entities.Animated;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.render.Renderable;

/**
 * Created by Marcus on 12/05/2015.
 */
public class C_Player implements Renderable, Animated {
    public static Array<Texture> textures;
    private Vector2 lastPos;
    public float x;
    public float y;
    private Vector2 size;
    public long id;


    public static final float PLAYER_SPEED = 10;

    public C_Player(float x, float y, long id){
        this.x = x;
        this.y = y;
        size = new Vector2(Global.TILE_SIZE, Global.TILE_SIZE);
        this.id = id;

        lastPos = new Vector2(x, y);



    }

    private int currentFrame = 0;
    private int animOffset = 0;
    private float frameTime = 0;

    public boolean updateMovement(){

        float tempx = x - lastPos.x;
        float tempy = y - lastPos.y;

        if(tempx == 0 && tempy == 0){
            animOffset = 0;
            lastPos.x = x;
            lastPos.y = y;

            return false;
        } else {

            if (tempx > 0 && tempx > tempy) {
                animOffset = 21;
            } else if (tempx < 0 && tempx < tempy) {
                animOffset = 14;
            }

            if (tempy > 0 && tempy > tempx) {
                animOffset = 7;
            } else if (tempy < 0 && tempy < tempx) {
                animOffset = 0;
            }
            lastPos.x = x;
            lastPos.y = y;
            return true;
        }


    }

    @Override
    public void updateAnimation(float delta) {
        frameTime += delta;
        if(frameTime > Global.PLAYER_ANIMATION_RATE){
            frameTime -= Global.PLAYER_ANIMATION_RATE;
            currentFrame++;
        }

        if(currentFrame >= 7){
            currentFrame = 0;
        }
    }

    @Override
    public void render(SpriteBatch renderer) {
        renderer.draw(textures.get(currentFrame + animOffset), x - size.x/2, y - size.y/4);
    }

    @Override
    public void render(ShapeRenderer renderer) {

    }

    public void loadAssets() {
        textures = new Array<Texture>();
        for (int i = 1; i <= 28; i++) {
            textures.add(new Texture(Gdx.files.internal("player/player_" + (i < 10 ? "0" : "") +i+".png")));
        }
    }

    public Vector2 getPosition(){
        return new Vector2(x, y);
    }
}
