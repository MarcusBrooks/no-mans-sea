package uk.co.greedygull.entities;

/**
 * Created by Marcus on 12/05/2015.
 */
public interface Animated {
    public void updateAnimation(float delta);
}
