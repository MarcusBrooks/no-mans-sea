package uk.co.greedygull.entities;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.utils.ImmutableArray;

/**
 * Created by Marcus on 09/04/2015.
 */
public interface FieldOfView {
    public ImmutableArray<Entity> getEntitiesInRadius(int rad);
}
