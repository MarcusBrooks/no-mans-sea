package uk.co.greedygull.entities.server;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import uk.co.greedygull.components.*;
import uk.co.greedygull.entities.client.C_Player;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.server.GameServer;

/**
 * Created by Marcus on 12/05/2015.
 */
public class S_Player extends Entity {

    private BodyDef bodydef;
    private BodyDef bodydef2;
    private Body body;
    private Body body2;
    public static Fixture attackFixture;
    public static Fixture bodyFixture;

    public S_Player(Vector2 pos, int id){
        this(pos.x, pos.y);
    }

    public S_Player(float x, float y){
        add(new PositionComponent(x, y));
        add(new SizeComponent(Global.TILE_SIZE));
        add(new HealthComponent());

        bodydef = new BodyDef();
        bodydef.type = BodyDef.BodyType.DynamicBody;
        bodydef.position.set(SteerableEntity.pixelsToMeters(x),SteerableEntity.pixelsToMeters(y));

        body = GameServer.world.createBody(bodydef);
        body.setTransform(SteerableEntity.pixelsToMeters(x),SteerableEntity.pixelsToMeters(y), body.getAngle());

        PolygonShape box = new PolygonShape();
        box.setAsBox(SteerableEntity.pixelsToMeters(Global.TILE_SIZE / 4),SteerableEntity.pixelsToMeters((Global.TILE_SIZE / 4)));
        body.createFixture(box, 0.0f);

        bodyFixture = body.createFixture(box, 0.0f);
        bodyFixture.setSensor(false);
        body.setUserData(this);

    }


    public void attack(){
        ImmutableArray<Entity> entities = GameServer.entityEngine.getEntitiesFor(Family.all(Box2DComponent.class).get());
        for(Entity e : entities){

            Vector2 tmp = ((GreenEntity)e).getPosition();
            System.out.println(tmp.dst(body.getPosition()));

            if(tmp.dst(body.getPosition()) < 2){
                System.out.println(((GreenEntity)e).health);
                ((GreenEntity)e).health -= 25;
                if(((GreenEntity)e).health <= 0){
                    GameServer.bodiesToDestroy.add( e.getComponent(Box2DComponent.class).body);
                }
                ((GreenEntity)e).setMaxLinearSpeed(15);
                e.getComponent(Box2DComponent.class).body.applyForceToCenter(e.getComponent(Box2DComponent.class).body.getPosition().sub(body.getPosition()).scl(20), true);
                e.getComponent(Box2DComponent.class).body.setLinearVelocity(e.getComponent(Box2DComponent.class).body.getPosition().sub(body.getPosition()).scl(20));


            }
        }
    }

    public void updateMovement(float x, float y){
        PositionComponent pos = Mapper.position.get(this);
        Vector2 vec = new Vector2(x, y);
        Vector2 temp = vec.scl(C_Player.PLAYER_SPEED);
        //body.applyForce(vec, body.getPosition(), false);
        body.setLinearVelocity(temp);

        //body2.getPosition().set(body.getPosition());

//        bodydef.position.set(pos.x, pos.y);
//        body.setTransform(pos.x, pos.y, body.getAngle());
    }

    public Vector2 getPosition(){
       return new Vector2(SteerableEntity.metersToPixels(body.getPosition().x), SteerableEntity.metersToPixels(body.getPosition().y));
    }

}
