package uk.co.greedygull.entities.server;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import uk.co.greedygull.components.Box2DComponent;
import uk.co.greedygull.components.MovementStateComponent;
import uk.co.greedygull.components.SizeComponent;
import uk.co.greedygull.main.Global;

/**
 * Created by Marcus on 14/04/2015.
 */
public class SteerableEntity extends Entity implements Steerable<Vector2> {
    private float boundingRadius;
    private boolean tagged;
    private float maxLinearSpeed;
    private float maxLinearAcceleration;
    private  float maxAngularSpeed;
    private float maxAngularAcceleration;
    private boolean independentFacing = false;
    private boolean isAttacking = false;
    protected SteeringBehavior<Vector2> steeringBehavior;
    public int health = 100;
//    protected boolean isTarget = false;

    private static final SteeringAcceleration<Vector2> steeringOutput = new SteeringAcceleration<Vector2>(new Vector2());

    /**
     * extends Entity implements Steerable<Vector2>
     * @param world box2D World
     * @param pos Vector position x y
     * @param bounds Bounding Radius
     * @param target true = tagged i.e. a STATIC Target; false = DYNAMIC;
     */
    public SteerableEntity(World world, Vector2 pos, float bounds, boolean target){
        this.boundingRadius = pixelsToMeters(bounds/4f);
       // this.tagged = false;
//        this.isTarget = target;
        setTagged(target);
//        PositionComponent positionComp = new PositionComponent();
//        positionComp.x = pos.x;
//        positionComp.y = pos.y;
//        add(positionComp);
        add(new SizeComponent(Global.TILE_SIZE/2));
        add(new MovementStateComponent());
        //Box2DComponent bodyComp =
        add(new Box2DComponent());

        initSteeringEntity(world, pos);

        Vector2 v = new Vector2(pixelsToMeters(pos.x),pixelsToMeters(pos.y));

        getComponent(Box2DComponent.class).body.setTransform(v, getComponent(Box2DComponent.class).body.getAngle());
        getComponent(Box2DComponent.class).body.setUserData(this);

    }

    public boolean isIndependentFacing () {
        return independentFacing;
    }

    public SteeringBehavior<Vector2> getSteeringBehavior () {
        return steeringBehavior;
    }

    public void setSteeringBehavior (SteeringBehavior<Vector2> steeringBehavior) {
        this.steeringBehavior = steeringBehavior;
    }

    @Override
    public Vector2 getPosition() {
        return getComponent(Box2DComponent.class).body.getPosition();
    }

    @Override
    public float getOrientation() {
        return getComponent(Box2DComponent.class).body.getAngle();
    }


    @Override
    public Vector2 getLinearVelocity() {
        return getComponent(Box2DComponent.class).body.getLinearVelocity();
    }

    @Override
    public float getAngularVelocity() {
        return getComponent(Box2DComponent.class).body.getAngularVelocity();
    }

    @Override
    public float getBoundingRadius() {
        return boundingRadius;
    }

    @Override
    public boolean isTagged() {
        return tagged;
    }

    @Override
    public void setTagged(boolean tagged) {this.tagged = tagged;    }

    @Override
    public Vector2 newVector() {
        return new Vector2();
    }


    @Override
    public float vectorToAngle(Vector2 vector) {
        return (float)Math.atan2(-vector.x, vector.y);
    }

    @Override
    public Vector2 angleToVector(Vector2 outVector, float angle) {
        outVector.x = -(float)Math.sin(angle);
        outVector.y = (float)Math.cos(angle);
        return outVector;
    }

    @Override
    public float getMaxLinearSpeed() {
        return maxLinearSpeed;
    }

    @Override
    public void setMaxLinearSpeed(float maxLinearSpeed) {this.maxLinearSpeed = maxLinearSpeed; }

    @Override
    public float getMaxLinearAcceleration() {
        return maxLinearAcceleration;
    }

    @Override
    public void setMaxLinearAcceleration(float maxLinearAcceleration) {this.maxLinearAcceleration = maxLinearAcceleration;    }

    @Override
    public float getMaxAngularSpeed() {
        return maxAngularSpeed;
    }

    @Override
    public void setMaxAngularSpeed(float maxAngularSpeed) {this.maxAngularSpeed = maxAngularSpeed;   }

    @Override
    public float getMaxAngularAcceleration() {
        return maxAngularAcceleration;
    }

    @Override
    public void setMaxAngularAcceleration(float maxAngularAcceleration) {this.maxAngularAcceleration = maxAngularAcceleration;    }

    public static float pixelsToMeters (float pixels) {
        return pixels * 0.02f;
    }

    public static int metersToPixels (float meters) {
        return (int)(meters * 50f);
    }

    public void initSteeringEntity (World world, Vector2 position) {

        // First we create a body definition
        BodyDef bodyDef = new BodyDef();
        // We set our body to dynamic, for something like ground which doesn't move we would set it to StaticBody
        if(isTagged()) {
            System.out.println("TAGGED!??");

            bodyDef.type = BodyDef.BodyType.StaticBody;
        }else {
            bodyDef.type = BodyDef.BodyType.DynamicBody;
        }
        // Set our body's starting position in the world
        bodyDef.position.set(pixelsToMeters(position.x), pixelsToMeters(position.y));

        // Create our body in the world using our body definition
        getComponent(Box2DComponent.class).body = world.createBody(bodyDef);

        // Create a circle shape and set its radius to 6
        CircleShape circle = new CircleShape();

        circle.setRadius(boundingRadius);
        circle.setPosition(new Vector2());

        // Create a fixture definition to apply our shape to
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = circle;

        fixtureDef.density = 1.0f;
        fixtureDef.filter.groupIndex = 0;
       fixtureDef.friction = 0.4f;
       fixtureDef.restitution = 0.6f; // Make it bounce a little bit

        getComponent(Box2DComponent.class).body.createFixture(fixtureDef);

        // Remember to dispose of any shapes after you're done with them!
        // BodyDef and FixtureDef don't need disposing, but shapes do.
        circle.dispose();
    }

//    public Box2dSteeringEntity createSteeringEntity (World world, TextureRegion region, boolean independentFacing, int posX, int posY) {
//
//        CircleShape circleChape = new CircleShape();
//        circleChape.setPosition(new Vector2());
//        int radiusInPixels = (int)((region.getRegionWidth() + region.getRegionHeight()) / 4f);
//        circleChape.setRadius(Box2dSteeringTest.pixelsToMeters(radiusInPixels));
//
//        BodyDef characterBodyDef = new BodyDef();
//        characterBodyDef.position.set(Box2dSteeringTest.pixelsToMeters(posX), Box2dSteeringTest.pixelsToMeters(posY));
//        characterBodyDef.type = BodyDef.BodyType.DynamicBody;
//        Body characterBody = world.createBody(characterBodyDef);
//
//        FixtureDef charFixtureDef = new FixtureDef();
//        charFixtureDef.density = 1;
//        charFixtureDef.shape = circleChape;
//        charFixtureDef.filter.groupIndex = 0;
//        characterBody.createFixture(charFixtureDef);
//
//        circleChape.dispose();
//
//        return new Box2dSteeringEntity(region, characterBody, independentFacing, Box2dSteeringTest.pixelsToMeters(radiusInPixels));
//    }

    public void update (float deltaTime) {
        if (steeringBehavior != null && steeringOutput != null) {
            // Calculate steering acceleration
            steeringBehavior.calculateSteering(steeringOutput);


			/*
			 * Here you might want to add a motor control layer filtering steering accelerations.
			 *
			 * For instance, a car in a driving game has physical constraints on its movement: it cannot turn while stationary; the
			 * faster it moves, the slower it can turn (without going into a skid); it can brake much more quickly than it can
			 * accelerate; and it only moves in the direction it is facing (ignoring power slides).
			 */

            // Apply steering acceleration
            applySteering(steeringOutput, deltaTime);
        }


    }

    protected void applySteering (SteeringAcceleration<Vector2> steering, float deltaTime) {
        boolean anyAccelerations = false;

        // Update position and linear velocity.
        if (!steeringOutput.linear.isZero()) {
            Vector2 force = steeringOutput.linear.scl(deltaTime);
            getComponent(Box2DComponent.class).body.applyForceToCenter(force, true);
            anyAccelerations = true;
        }

        // Update orientation and angular velocity
        if (isIndependentFacing()) {
            if (steeringOutput.angular != 0) {
//                System.out.println("aa: " + steeringOutput.angular);
                getComponent(Box2DComponent.class).body.applyTorque(pixelsToMeters(steeringOutput.angular) * deltaTime , true);
                anyAccelerations = true;
            }
        }
        else {
            // If we haven't got any velocity, then we can do nothing.
            Vector2 linVel = getLinearVelocity();
            if (!linVel.isZero(getZeroLinearSpeedThreshold())) {
                float newOrientation = vectorToAngle(linVel);
                getComponent(Box2DComponent.class).body.setAngularVelocity((newOrientation - getAngularVelocity()) * deltaTime); // this is superfluous if independentFacing is always true
                getComponent(Box2DComponent.class).body.setTransform(getComponent(Box2DComponent.class).body.getPosition(), newOrientation);
            }
        }

        if (anyAccelerations) {
//             body.activate();

            // TODO:
            // Looks like truncating speeds here after applying forces doesn't work as expected.
            // We should likely cap speeds form inside an InternalTickCallback, see
            // http://www.bulletphysics.org/mediawiki-1.5.8/index.php/Simulation_Tick_Callbacks

            // Cap the linear speed
            Vector2 velocity = getComponent(Box2DComponent.class).body.getLinearVelocity();
            float currentSpeedSquare = velocity.len2();
            float maxLinearSpeed = getMaxLinearSpeed();
            if (currentSpeedSquare > maxLinearSpeed * maxLinearSpeed) {
                getComponent(Box2DComponent.class).body.setLinearVelocity(velocity.scl(maxLinearSpeed / (float)Math.sqrt(currentSpeedSquare)));
            }

            // Cap the angular speed
            float maxAngVelocity = getMaxAngularSpeed();
            if (getComponent(Box2DComponent.class).body.getAngularVelocity() > maxAngVelocity) {
                getComponent(Box2DComponent.class).body.setAngularVelocity(maxAngVelocity);
            }
        }
    }

    public float getZeroLinearSpeedThreshold () {
        return 0.001f;
    }

    public void markAsSensor () {
//        Array<Fixture> fixtures = this.getComponent(Box2DComponent.class).body.getFixtureList();
//        for (int i = 0; i < fixtures.size; i++) {
//            fixtures.get(i).setSensor(true);
//        }
            for(Fixture f: this.getComponent(Box2DComponent.class).body.getFixtureList()){
                f.setSensor(true);
            }

    }

}
