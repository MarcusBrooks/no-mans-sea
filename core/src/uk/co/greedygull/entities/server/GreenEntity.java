package uk.co.greedygull.entities.server;

import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.behaviors.*;
import com.badlogic.gdx.ai.steer.limiters.NullLimiter;
import com.badlogic.gdx.ai.steer.utils.rays.CentralRayWithWhiskersConfiguration;
import com.badlogic.gdx.ai.utils.Ray;
import com.badlogic.gdx.ai.utils.RaycastCollisionDetector;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import uk.co.greedygull.components.Box2DComponent;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.server.GameServer;

/**
 * Created by Marcus on 14/04/2015.
 */
public class GreenEntity extends SteerableEntity {

    //    private SteerableEntity target;
    public Wander<Vector2> wanderSB;
    public PrioritySteering<Vector2> prioritySteeringSB;
    RaycastObstacleAvoidance<Vector2> raycastObstacleAvoidanceSB;
    CentralRayWithWhiskersConfiguration<Vector2> rayConfig;
    ProximityObj proximity;
    private boolean hasTarget = false;
    public int health = 100;

    public GreenEntity(World world, Vector2 pos) {
        super(world, pos, Global.TILE_SIZE, false);//TODO change 128 to width of texture (now TILE_SIZE)
        setMaxLinearSpeed(2);
        setMaxLinearAcceleration(100);
        setMaxAngularAcceleration(100); // greater than 0 because independent facing is enabled
        setMaxAngularSpeed(20);

        getComponent(Box2DComponent.class).body.setUserData(this);

        this.wanderSB = new Wander<Vector2>(this) //
//                .setFaceEnabled(true) //
//                .setAlignTolerance(0.001f)
//                .setDecelerationRadius(5)
//                .setTimeToTarget(0.1f)
                .setFaceEnabled(false) //
                        // We don't need a limiter supporting angular components because Face is not used
                        // No need to call setAlignTolerance, setDecelerationRadius and setTimeToTarget for the same reason
//                .setLimiter(new LinearAccelerationLimiter(10)) //
                .setWanderOffset(90) //
                .setWanderOrientation(10) //
                .setWanderRadius(80) //
                .setWanderRate((MathUtils.PI/4));

        proximity = new ProximityObj(this, world, getBoundingRadius() * 10);
//        System.out.println("Bounds:"+getBoundingRadius()*10);

        rayConfig = new CentralRayWithWhiskersConfiguration<Vector2>(this, pixelsToMeters(100),
                pixelsToMeters(40), 35 * MathUtils.degreesToRadians);

        RaycastCollisionDetector<Vector2> raycastCollisionDetector = new Box2dRaycastCollisionDetector(world);

        raycastObstacleAvoidanceSB = new RaycastObstacleAvoidance<Vector2>(this, rayConfig,
                raycastCollisionDetector, 1);
        //raycastObstacleAvoidanceSB.setDistanceFromBoundary(1);

        CollisionAvoidance<Vector2> collisionAvoidanceSB = new CollisionAvoidance<Vector2>(this, proximity){
            @Override
            public boolean reportNeighbor(Steerable<Vector2> neighbor) {
                if(neighbor.isTagged() /*&& !hasTarget()*/){ // if neighbour has been tagged as a target then attack their target?
                    SteerableEntity target = new SteerableEntity(GameServer.world, new Vector2(metersToPixels(neighbor.getPosition().x),metersToPixels(neighbor.getPosition().y) ), 10, true);
                    setSeek(target);
                    neighbor.setTagged(true);
                    System.out.println("FOUND A TAGGED OBJECT - SEEKING");
                    System.out.println("TargetPos:"+neighbor.getPosition().toString());

                }
                return super.reportNeighbor(neighbor);
            }
        };

        prioritySteeringSB = new PrioritySteering<Vector2>(this, 0.0001f);
        prioritySteeringSB.add(raycastObstacleAvoidanceSB);
        prioritySteeringSB.add(wanderSB);

        setSteeringBehavior(prioritySteeringSB);
    }

    public void setWander(){
        this.setSteeringBehavior(wanderSB);
        this.hasTarget = false;
    }

    public Ray<Vector2>[] getRaycastWhiskers(){
        return rayConfig.getRays();
    }

    public void setSeek(SteerableEntity target){

        final LookWhereYouAreGoing<Vector2> lookWhereYouAreGoingSB = new LookWhereYouAreGoing<Vector2>(this) //
                .setTimeToTarget(0.1f) //
                .setAlignTolerance(0.001f) //
                .setDecelerationRadius(MathUtils.PI);

        final Arrive<Vector2> arriveSB = new Arrive<Vector2>(this, target) //
                .setTimeToTarget(0.1f) //
                .setArrivalTolerance(0.0002f) //
                .setDecelerationRadius(3);

        BlendedSteering<Vector2> blendedSteering = new BlendedSteering<Vector2>(this) //
                .setLimiter(NullLimiter.NEUTRAL_LIMITER) //
                .add(arriveSB, 1f) //
                .add(lookWhereYouAreGoingSB, 2f)
                .add(this.wanderSB, 0.2f);
        this.setSteeringBehavior(blendedSteering);
        this.hasTarget = true;
    }
    public void setAvoid(){
        setSteeringBehavior(wanderSB);
    }

    public boolean hasTarget(){
        return hasTarget;
    }

    Float totalTime = 0f;
    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        if(getMaxLinearSpeed() > 2) {
            totalTime += deltaTime;

            if (totalTime >= 0.5f) {
                totalTime -= 0.5f;
                setMaxLinearSpeed(2);
            }
        }


        //TODO if player is closeby, set seek to player location
    }
}
