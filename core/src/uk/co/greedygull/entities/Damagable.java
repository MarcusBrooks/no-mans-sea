package uk.co.greedygull.entities;

/**
 * Created by Marcus on 09/04/2015.
 */
public interface Damagable {
    public void takeDamage(int dmg);
}
