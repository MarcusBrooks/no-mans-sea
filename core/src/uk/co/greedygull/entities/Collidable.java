package uk.co.greedygull.entities;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;


/**
 * Created by Marcus on 03/04/2015.
 */
public interface Collidable {
    public boolean collidesWith(Rectangle r);
    public boolean collidesWith(Vector2 v);
}
