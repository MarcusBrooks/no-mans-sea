package uk.co.greedygull.entities;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by Marcus on 08/04/2015.
 */
public interface Moveable {
    public void translate(Vector2 vec);
}
