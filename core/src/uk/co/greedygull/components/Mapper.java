package uk.co.greedygull.components;

import com.badlogic.ashley.core.ComponentMapper;

/**
 * Created by Marcus on 09/04/2015.
 */
public class Mapper {
    //public static final ComponentMapper<PositionComponent> position = ComponentMapper.getFor(PositionComponent.class);
    public static final ComponentMapper<SizeComponent> size = ComponentMapper.getFor(SizeComponent.class);
    public static final ComponentMapper<MovementStateComponent> movement = ComponentMapper.getFor(MovementStateComponent.class);
    public static final ComponentMapper<Box2DComponent> box2d = ComponentMapper.getFor(Box2DComponent.class);
    public static final ComponentMapper<PositionComponent> position = ComponentMapper.getFor(PositionComponent.class);


}
