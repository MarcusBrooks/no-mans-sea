package uk.co.greedygull.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Marcus on 09/04/2015.
 */
public class SizeComponent extends Component {
    public SizeComponent(int width, int height){
        this.width = width;
        this.height = height;
    }

    public SizeComponent(int size){
        this.width = size;
        this.height = size;
    }

    public int width = 0;
    public int height = 0;
}