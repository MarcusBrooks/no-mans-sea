package uk.co.greedygull.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Created by Marcus on 14/04/2015.
 */
public class Box2DComponent extends Component {
    public Body body;
}
