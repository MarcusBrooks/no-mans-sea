package uk.co.greedygull.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Marcus on 09/04/2015.
 */
public class PositionComponent extends Component {

    public PositionComponent(float x, float y){
        this.x = x;
        this.y = y;
    }

    public float x = 0;
    public float y = 0;
}
