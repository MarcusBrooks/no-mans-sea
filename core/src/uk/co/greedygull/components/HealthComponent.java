package uk.co.greedygull.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Marcus on 09/04/2015.
 */
public class HealthComponent extends Component {
    public int health = 100;
}
