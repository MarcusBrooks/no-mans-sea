package uk.co.greedygull.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.ai.steer.behaviors.Wander;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Marcus on 14/04/2015.
 */
public class MovementStateComponent extends Component {
    public enum MovementState {
        WANDER, SEEK, AVOID, PROTECT
    }
    private MovementState currentMovementState = MovementState.WANDER;

}
