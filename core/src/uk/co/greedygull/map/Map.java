package uk.co.greedygull.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import uk.co.greedygull.entities.server.SteerableEntity;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.render.Renderable;
import uk.co.greedygull.server.GameServer;

import java.util.ArrayList;
import java.util.Random;

import static uk.co.greedygull.main.Global.*;

/**
 * Created by Marcus on 03/04/2015.
 */
public class Map implements Renderable {
    private byte[][] map;
    private byte[][] textureMap;
    private static Array<Texture> tiles = new Array<Texture>();
    private World world = GameServer.world;
    private ArrayList<Vector2> freeSpace = new ArrayList<Vector2>();



    public Map() {
        map = initCellularAutomata();

        textureMap = initTextureMap(map);
    }

    public void initWorld(){

        int boxCounter = 0;

        for (int x = 0; x < MAP_WIDTH; x++) {
            for (int y = 0; y < MAP_HEIGHT; y++) {

                boolean makeBox = false;


                if(x > 5 && x < MAP_WIDTH - 5){
                    if(y > 5 && y < MAP_WIDTH - 5){

                        if(map[x][y] == 2) {
                            freeSpace.add(new Vector2(x, y));
                        }
                    }
                }

                if(map[x][y] != 2){

                    if(x < MAP_WIDTH-1 && map[x+1][y] == 2){
                        makeBox = true;
                    } else if(y < MAP_HEIGHT-1 && map[x][y+1] == 2){
                        makeBox = true;
                    } else if(x > 0 && map[x-1][y] == 2){
                        makeBox = true;
                    } else if(y > 0 && map[x][y-1] == 2){
                        makeBox = true;
                    }

                    if(makeBox) {
                        initBox2d(new Vector2(x, y));
                        boxCounter++;
                    }
                }

            }
        }
        System.out.println("# of BOXES: "+boxCounter);

    }

    private void initBox2d(Vector2 v){
        //Create body definition
        BodyDef bodyDef = new BodyDef();
        //Define body type as static
        bodyDef.type = BodyDef.BodyType.StaticBody;
        //Set its world position

        float tempx = (v.x)*Global.TILE_SIZE + Global.TILE_SIZE/2;
        float tempy = (v.y)*Global.TILE_SIZE + Global.TILE_SIZE/2;

        Vector2 pos = new Vector2(SteerableEntity.pixelsToMeters(tempx),SteerableEntity.pixelsToMeters(tempy));
        bodyDef.position.set(pos);

        //Create a body from the definition and add into the world
        Body body = world.createBody(bodyDef);
        body.setTransform(pos, body.getAngle());

        //Create a polygon shape
        PolygonShape box = new PolygonShape();
        //Set shape as a 1x1 box
        box.setAsBox(SteerableEntity.pixelsToMeters(Global.TILE_SIZE / 2),SteerableEntity.pixelsToMeters(Global.TILE_SIZE / 2));

        //Create a fixture from the polygon shape and add to body
        body.createFixture(box, 0.0f);

    }

    public byte[][] initTextureMap(byte[][] map){
        byte[][] tempMap = new byte[MAP_WIDTH][MAP_HEIGHT];

        for (int x = 0; x < MAP_WIDTH; x++) {
            for (int y = 0; y < MAP_HEIGHT; y++) {

                if(map[x][y] == 2){
                    tempMap[x][y] = 0;
                    continue;
                }

                byte mask = 0;
                if(y < MAP_HEIGHT - 1 && map[x][y+1] != 2) {
                    mask |= 1;
                }

                if(x < MAP_WIDTH - 1 && map[x+1][y] != 2){
                    mask |= (1 << 1);
                }

                if(y > 0 && map[x][y-1] != 2){
                    mask |= (1 << 2);
                }
                if(x > 0 && map[x-1][y] != 2){
                    mask |= (1 << 3);
                }

                tempMap[x][y] = mask;

            }
        }

        return tempMap;
    }

    public Map(byte[][] serverMap) {
        map = serverMap;
        textureMap = initTextureMap(serverMap);
    }


    public byte[][] initCellularAutomata(){
        byte[][] tempMap = new byte[MAP_WIDTH][MAP_HEIGHT];
        Random rand = new Random();

        //Initialise grid with 45% chance of walls
        for (int x = 0; x < MAP_WIDTH; x++) {
            for (int y = 0; y < MAP_HEIGHT; y++) {
                if(rand.nextFloat() < 0.40f){
                    tempMap[x][y] = 1;
                } else {
                    tempMap[x][y] = 0;
                }
            }
        }

        //For each tile, if it is surrounded by 5 walls, then it is a wall. Otherwise it is not.

        for (int i = 0; i < 10; i++) {
            byte[][] tempTempMap = new byte[MAP_WIDTH][MAP_HEIGHT];

            for (int x = 0; x < MAP_WIDTH; x++) {
                for (int y = 0; y < MAP_HEIGHT; y++) {

                    int tileCount = countNeighbours(tempMap, x, y);
                    if (tileCount >= 5 || tileCount <= 2) {
                        tempTempMap[x][y] = 1;
                    } else {
                        tempTempMap[x][y] = 0;
                    }

                    if(tempMap[x][y] == 1){
                        if(tileCount < 3){
                            tempTempMap[x][y] = 0;
                        }
                        else{
                            tempTempMap[x][y] = 1;
                        }
                    } //Otherwise, if the cell is dead now, check if it has the right number of neighbours to be 'born'
                    else{
                        if(tileCount > 4){
                            tempTempMap[x][y] = 1;
                        }
                        else{
                            tempTempMap[x][y] = 0;
                        }
                    }

                }

            }
            tempMap = tempTempMap;
        }

        for (int x = 0; x < MAP_WIDTH-1; x++) {
            for (int y = 0; y < MAP_HEIGHT-1; y++) {
                if(tempMap[x][y] == 1 && tempMap[x+1][y] == 0 && tempMap[x][y+1] == 0 && tempMap[x+1][y+1] == 1){
                    tempMap[x][y] = 0;
                    tempMap[x+1][y+1] = 0;
                }
                if(tempMap[x][y] == 0 && tempMap[x+1][y] == 1 && tempMap[x][y+1] == 1 && tempMap[x+1][y+1] == 0){
                    tempMap[x+1][y] = 0;
                    tempMap[x][y+1] = 0;
                }
            }
        }

        boolean bigSpace = false;
        int floodAttempts = 0;
        while(!bigSpace) {
            int innerFloodAttempts = 0;
            while (true) {
                floodCount = 0;
                if (floodFill(tempMap, rand.nextInt(MAP_WIDTH - 1), rand.nextInt(MAP_HEIGHT - 1), (byte)0, (byte)2) || innerFloodAttempts > 10) {
                    break;
                } else {
                    innerFloodAttempts++;
                }
            }

            if(floodCount > (MAP_WIDTH * MAP_HEIGHT) / 2){
                bigSpace = true;
                for (int x = 0; x < MAP_WIDTH; x++) {
                    for (int y = 0; y < MAP_HEIGHT; y++) {
                        if(tempMap[x][y] == 0){
                            tempMap[x][y] = 1;
                        }
                    }
                }
            } else if (floodAttempts < 5){
                for (int x = 0; x < MAP_WIDTH; x++) {
                    for (int y = 0; y < MAP_HEIGHT; y++) {
                        if(tempMap[x][y] == 2){
                            tempMap[x][y] = 1;
                        }
                    }
                }
                floodAttempts++;
            } else {
                tempMap = initCellularAutomata();
                bigSpace = true;
            }
        }

        return tempMap;
    }

    int floodCount = 0;
    public boolean floodFill(byte[][] map, int x, int y, byte target, byte replacement){

        if (map[x][y] != target) return false;

        map[x][y] = replacement;
        floodCount++;
        if(x != 0 && x != MAP_WIDTH-1 && y != 0 && y != MAP_HEIGHT-1) {
            floodFill(map, x - 1, y, target, replacement);
            floodFill(map, x + 1, y, target, replacement);
            floodFill(map, x, y - 1, target, replacement);
            floodFill(map, x, y + 1, target, replacement);
        }
        return true;
    }

    public int countNeighbours(byte[][] map, int x, int y){
        int count = 0;
        for(int i=-1; i<2; i++){
            for(int j=-1; j<2; j++){
                int neighbour_x = x+i;
                int neighbour_y = y+j;
                //If we're looking at the middle point
                if(i == 0 && j == 0){
                    //Do nothing, we don't want to add ourselves in!
                }
                //In case the index we're looking at it off the edge of the map
                else if(neighbour_x < 0 || neighbour_y < 0 || neighbour_x >= map.length || neighbour_y >= map[0].length){
                    count = count + 2;
                }
                //Otherwise, a normal check of the neighbour
                else if(map[neighbour_x][neighbour_y] == 1){
                    count = count + 1;
                }
            }
        }
        return count;
    }

    public void render(final SpriteBatch renderer) {
        for (int x = 0; x < MAP_WIDTH; x++) {
            for (int y = 0; y < MAP_HEIGHT; y++) {

                renderer.draw(tiles.get(textureMap[x][y]), x * Global.TILE_SIZE, y * Global.TILE_SIZE);

            }
        }
    }

    public void render(final ShapeRenderer debug) {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

        for (int x = 0; x < MAP_WIDTH; x++) {
            for (int y = 0; y < MAP_HEIGHT; y++) {
                if(textureMap[x][y] == 0) {
                    debug.setColor(0,1,0,0.2f);

                    debug.rect((x * TILE_SIZE),
                            (y * TILE_SIZE),
                            TILE_SIZE,
                            TILE_SIZE);
                } else {
                    debug.setColor(1,0,0,0.2f);

                    debug.rect((x * TILE_SIZE),
                            (y * TILE_SIZE),
                            TILE_SIZE,
                            TILE_SIZE);
                }
            }
        }
    }

    public final byte[][] getArray(){
        return map;
    }

    public ArrayList<Vector2> getFreeSpace(){
        return freeSpace;
    }

    public void loadAssets(){
        for (int i = 0; i < 16; i++) {
            tiles.add(new Texture(Gdx.files.internal("tiles/"+i+".png")));
        }

    }
}