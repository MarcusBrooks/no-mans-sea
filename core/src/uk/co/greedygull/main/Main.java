package uk.co.greedygull.main;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import uk.co.greedygull.screens.GameScreen;
import uk.co.greedygull.server.*;
import uk.co.greedygull.screens.MenuScreen;

/**
 * Created by Marcus on 01/04/2015.
 */
public class Main extends Game {
    //TODO Setup preference saving
    private static Preferences prefs;

    @Override
	public void create() {
        prefs = Gdx.app.getPreferences("Preferences");

        setScreen(new MenuScreen());
	}

    @Override
	public void render() {
        super.render();
    }
}
