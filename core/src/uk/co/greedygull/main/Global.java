package uk.co.greedygull.main;

/**
 * Created by Marcus on 01/04/2015.
 */
public class Global {

    public static final int TILE_SIZE = 124;

    public static final int CAMERA_WIDTH = 1280;
    public static final int CAMERA_HEIGHT = 720;

    public static final float CAMERA_ZOOM = 1.0f;

    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;

    public static final int MAP_WIDTH = 40;
    public static final int MAP_HEIGHT = 40;

    public static final String IP_ADDRESS = "localhost";
    public static final int GLOBAL_TIMEOUT = 5000;
    public static final int MIN_PORT = 54555;
    public static final int MAX_PORT = 54777;
    
    public static final int NUM_ENTITIES = 20;

    public static final float PLAYER_ANIMATION_RATE = 0.2f;


    public static final int ENEMY_FOV = 50; //TODO

    public static final float ENTITY_UPDATE_DELAY = 0.0f;
    public static final float PLAYER_UPDATE_DELAY = 0.0f;

}