package uk.co.greedygull.server;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import java.io.IOException;

import static uk.co.greedygull.main.Global.*;

/**
 * Created by Marcus on 08/04/2015.
 */
public class GameClient extends Client {

    EntityResponse latestEntityResponse;
    MapResponse mapResponse;
    PlayerResponse playerResponse;

    BasicPlayer initialPlayer;
    public boolean entityloaded = false;
    public boolean maploaded = false;
    public boolean playerloaded = false;
    public boolean playersloaded = false;

    public GameClient(){
        super();
        startClient();
    }

    public void startClient() {
        try {
            this.start();
            this.connect(GLOBAL_TIMEOUT, IP_ADDRESS, MIN_PORT, MAX_PORT);


            this.addListener(new Listener() {
                public void received(final Connection connection, final Object object) {

                    if (object instanceof TextResponse) {
                        TextResponse textResponse = (TextResponse) object;
                        System.out.println(textResponse.text);
                    } else if(object instanceof MapResponse){
                        mapResponse = (MapResponse)object;
                        maploaded = true;
                    } else if(object instanceof EntityResponse){
                        latestEntityResponse = (EntityResponse) object;
                        entityloaded = true;
                    } else if(object instanceof PlayerResponse){
                        playerResponse = (PlayerResponse)object;
                        playersloaded = true;

                    } else if(object instanceof BasicPlayer){
                        initialPlayer = (BasicPlayer)object;
                        playerloaded = true;
                    }
                }
            });

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    public byte[][] getMap(){
        return mapResponse.map;
    }

    public void registerClientClass(final Class var) {
        this.getKryo().register(var);
    }

}
