package uk.co.greedygull.server;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import uk.co.greedygull.entities.server.GreenEntity;
import uk.co.greedygull.entities.server.S_Player;
import uk.co.greedygull.entities.server.SteerableEntity;
import uk.co.greedygull.main.Global;
import uk.co.greedygull.map.Map;
import uk.co.greedygull.systems.AISystem;
import uk.co.greedygull.systems.EntityPackingSystem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import static uk.co.greedygull.main.Global.*;


/**
 * Created by Marcus on 08/04/2015.
 */
public class GameServer extends Server {

    public GameServer(){
        super();
        startServer();
    }

    Map map;
    public static Engine entityEngine;
    public static World world;
    Random rand;
    Array<S_Player> players = new Array<S_Player>();
//    S_Player player;
//    public static S_Player player;

    AISystem aiSystem;
    EntityPackingSystem entityPacker;

    public static Array<Body> bodiesToDestroy;
    public void startServer() {
        try {
            this.start();
            this.bind(MIN_PORT, MAX_PORT);
            rand = new Random();
            createWorld();
            bodiesToDestroy = new Array<Body>();
            System.out.print("CreatedWorld");

            this.addListener(new Listener() {
                public void received(final Connection connection, final Object object) {

                    if(object instanceof AttackRequest){
                        for(int i = 0; i < players.size; i++) {
                            if(((AttackRequest) object).id ==  players.get(i).getId()){
                                players.get(i).attack();
                            }
                        }
                    } else if (object instanceof BasicPlayer){
                        PlayerResponse playerResponse = new PlayerResponse();
                        playerResponse.players = new BasicPlayer[players.size];

                        for(int i = 0; i < players.size; i++) {
                            if(((BasicPlayer) object).id ==  players.get(i).getId()){

                                players.get(i).updateMovement(((BasicPlayer) object).x, ((BasicPlayer) object).y);
                                Vector2 pos = players.get(i).getPosition();

                            }


                            playerResponse.players[i] = new BasicPlayer();
                            playerResponse.players[i].x = players.get(i).getPosition().x;
                            playerResponse.players[i].y = players.get(i).getPosition().y;
                            playerResponse.players[i].id = players.get(i).getId();
                        }

                        connection.sendTCP(playerResponse);

                    } else if (object instanceof TextRequest) {
                        TextRequest textRequest = (TextRequest) object;

                        if (textRequest.text.equals("map")) {
                            MapResponse mapResponse = new MapResponse();
                            mapResponse.map = map.getArray();
                            connection.sendTCP(mapResponse);

                        } else if (textRequest.text.equals("entities")) {
                            EntityResponse entityResponse = entityEngine.getSystem(EntityPackingSystem.class).response;
                            connection.sendTCP(entityResponse);
                        } else if (textRequest.text.equals("player")) {

                            Random rand = new Random();
                            ArrayList<Vector2> freeSpace = map.getFreeSpace();
                            Vector2 playertemp = freeSpace.remove(rand.nextInt(freeSpace.size()));

                            S_Player newPlayer = new S_Player(playertemp.x * Global.TILE_SIZE, playertemp.y * Global.TILE_SIZE);
                            entityEngine.addEntity(newPlayer);
                            players.add(newPlayer);

                            BasicPlayer player = new BasicPlayer();
                            player.x =  SteerableEntity.metersToPixels(newPlayer.getPosition().x);
                            player.y =  SteerableEntity.metersToPixels(newPlayer.getPosition().y);
                            player.id = newPlayer.getId();
                            connection.sendTCP(player);

                        } else if (textRequest.text.equals("items")) {
                        } else {
                            TextResponse textResponse = new TextResponse();
                            textResponse.text = "Test Response";
                            connection.sendTCP(textResponse);
                        }
                    }


                }
            });

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void createWorld(){

        world = new World(new Vector2(0, 0), true);
        map = new Map();
        map.initWorld();

        aiSystem = new AISystem();
        entityPacker = new EntityPackingSystem();
        entityEngine = new Engine();
        entityEngine.addSystem(aiSystem);
        entityEngine.addSystem(entityPacker);

        ArrayList<Vector2> freeSpace = map.getFreeSpace();
        for (int i = 0; i < Global.NUM_ENTITIES; i++) {
            Vector2 temp = freeSpace.remove(rand.nextInt(freeSpace.size()));
            entityEngine.addEntity( new GreenEntity(world, new Vector2(temp.x * TILE_SIZE+TILE_SIZE/2, temp.y *TILE_SIZE+TILE_SIZE/2 )));//TODO take out the spawn limits
        }

        entityPacker.update(Gdx.graphics.getDeltaTime());

    }

    public void update(float delta){

            entityEngine.update(delta);
            world.step(delta, 6, 3);

        destroyBodies();

    }

    public void destroyBodies(){
        int i = 0;
        for(Body b : bodiesToDestroy){
            world.destroyBody(b);
            bodiesToDestroy.removeIndex(i);
            i++;
        }
    }
    public void registerServerClass(final Class var) {
        this.getKryo().register(var);
    }

}
