package uk.co.greedygull.server;

import com.badlogic.gdx.ai.steer.behaviors.Wander;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Marcus on 14/04/2015.
 */
public class BasicEntity {

    public float x;
    public float y;
    public long id;
    public float rotation;

}
