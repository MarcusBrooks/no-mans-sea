package uk.co.greedygull.server;

/**
 * Created by Marcus on 01/04/2015.
 */
public class Network {

    private GameClient client;
    private GameServer server;
    private static boolean isHosting;



    public Network(final boolean isHost) {
        isHosting = isHost;
        if (isHost) server = new GameServer();
        client = new GameClient();
    }

    public void send(String text){
        TextRequest textRequest = new TextRequest();
        textRequest.text = text;
        client.sendTCP(textRequest);
    }

    public void send(AttackRequest req){
        client.sendTCP(req);
    }

    public void send(BasicPlayer pr){
        client.sendTCP(pr);
    }

    public PlayerResponse getPlayerReponse(){
        while(!client.playersloaded){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return client.playerResponse;
    }

    public BasicPlayer getInitialPlayer(){
        while(!client.playerloaded){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return client.initialPlayer;
    }

    public EntityResponse getEntities(){
        while(!client.entityloaded){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return client.latestEntityResponse;
    }

    public byte[][] getMap(){
        while(!client.maploaded){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return client.mapResponse.map;
    }

    public void testNetwork() {
        long start = System.currentTimeMillis();
        TextRequest textRequest = new TextRequest();
        textRequest.text = "Test Request";
        client.sendTCP(textRequest);
        long end = System.currentTimeMillis();
        System.out.println("Initial Ping - " + (end - start));
    }

    public void updateServer(float delta){
        if(isHosting){
            server.update(delta);
        }
    }

    public void registerServerClass(final Class var) { server.registerServerClass(var); }

    public void registerClientClass(final Class var) { client.registerClientClass(var); }


    public GameServer getServer() {
        return server;
    }

}
