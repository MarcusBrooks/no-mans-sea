package mikera.data;

import mikera.annotations.Mutable;

import java.io.Serializable;

@Mutable
public class MutableObject implements Cloneable, Serializable {
	private static final long serialVersionUID = -4651948305928784088L;

	public MutableObject clone() {
		try {
			return (MutableObject) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new Error(e);
		}
	}
}
